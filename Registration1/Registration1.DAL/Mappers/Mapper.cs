﻿using Registration1.BL.ViewModels;

namespace Registration1.DAL.Mappers
{
    public static class Mapper
    {
        public static UserEntity ToEntity(UserViewModel userViewModel)
        {
            var result = new UserEntity
            {
                Birthday = userViewModel.Birthday,
                Email = userViewModel.Email,
                Gender = userViewModel.Gender,
                Id = userViewModel.Id,
                Name = userViewModel.Name,
                Password = userViewModel.Password,
                PhoneNumber = userViewModel.PhoneNumber,
                SecondName = userViewModel.SecondName
            };
            return result;

        }
        public static UserViewModel ToViewModel(UserEntity userEntity)
        {
            var result = new UserViewModel
            {
                Birthday = userEntity.Birthday,
                Email = userEntity.Email,
                Gender = userEntity.Gender,
                Id = userEntity.Id,
                Name = userEntity.Name,
                Password = userEntity.Password,
                PhoneNumber = userEntity.PhoneNumber,
                SecondName = userEntity.SecondName
            };
            return result;
        }
    }
}
