﻿using Registration1.BL.Interfaces;
using Registration1.BL.ViewModels;
using System.Collections.Generic;
using Registration1.DAL.Mappers;

namespace Registration1.DAL.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly RegistrationDbContext _db;
        private readonly BaseEntityRepository<UserEntity> _baseEntityRepository;

        public UserRepository(RegistrationDbContext dbContext)
        {
            _db = dbContext;

            _baseEntityRepository = new BaseEntityRepository<UserEntity>(_db);
        }

        public bool Create(UserViewModel item)
        {

            return _baseEntityRepository.Create(Mapper.ToEntity(item));
        }

        public bool Delete(int id)
        {
            return _baseEntityRepository.Delete(id);
        }

        public UserViewModel Get(int id)
        {
            var entity = _baseEntityRepository.Get(id);
            var result = Mapper.ToViewModel(entity);
            return result;

        }

        public List<UserViewModel> GetList()
        {
            List<UserViewModel> result = new List<UserViewModel>();
            var list = _baseEntityRepository.GetList();
            foreach (var n in list)
            {
                result.Add(Mapper.ToViewModel(n));
            }
            return result;
        }

        public bool Update(UserViewModel item)
        {
            var result = _baseEntityRepository.Update(Mapper.ToEntity(item));

            return result;

        }
    }
}
