﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Registration1.DAL.Repositories
{
    public class BaseEntityRepository<TEntity>
        where TEntity : class
    {
        private readonly RegistrationDbContext _db;

        public BaseEntityRepository(RegistrationDbContext dbContext)
        {
            _db = dbContext;
        }

        public bool Create(TEntity entity)
        {
            try
            {
                _db.Set<TEntity>().Add(entity);
                _db.SaveChanges();

                return true;
            }

            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var entity = _db.Set<TEntity>().Find(id);

                if (entity != null)
                {
                    _db.Set<TEntity>().Remove(entity);
                    _db.SaveChanges();

                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public TEntity Get(int id)
        {
            try
            {
                return _db.Set<TEntity>().Find(id);
            }
            catch(Exception)
            {
                return null;
            }
        }

        public List<TEntity> GetList()
        {
            try
            {
                return _db.Set<TEntity>().ToList();
            }
            catch (Exception)
            {
                return null;
            }

        }

        public bool Update(TEntity entity)
        {
            try
            {
                _db.Entry<TEntity>(entity).State = EntityState.Modified;
                _db.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;   
            }
        }
    }
}

