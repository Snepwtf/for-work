﻿using System.Data.Entity;

namespace Registration1.DAL
{
    public class RegistrationDbContext : DbContext
    {
        public RegistrationDbContext() : base("DBConnection") { }

        public DbSet<UserEntity> Users { get; set; }
        public DbSet<PassportEntity> Passports { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserEntity>().Property(p => p.Name).IsRequired();
            modelBuilder.Entity<UserEntity>().Property(p => p.Password).IsRequired();
            modelBuilder.Entity<UserEntity>().Property(p => p.PhoneNumber).IsRequired();
            modelBuilder.Entity<UserEntity>().Property(p => p.SecondName).IsRequired();

            modelBuilder.Entity<PassportEntity>().Property(p => p.PassportData).IsRequired();
            modelBuilder.Entity<PassportEntity>().Property(p => p.PassportNumber).IsRequired();

            modelBuilder.Entity<UserEntity>()
                .HasRequired(_ => _.Passport)
                .WithRequiredPrincipal(_ => _.User);

        }
    }
}
