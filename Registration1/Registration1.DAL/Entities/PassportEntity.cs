﻿using System;

namespace Registration1.DAL
{
    public class PassportEntity
    {
        public int Id { get; set; }
        public int PassportNumber { get; set; }

        public DateTime PassportData { get; set; }

        /// <summary>
        /// Навигационное свойство
        /// </summary>
        public UserEntity User { get; set; }
    }
}
