﻿using Registration1.BL.ViewModels;
using System.Collections.Generic;

namespace Registration1.BL.Interfaces.Managers
{
    public interface IUserManager
    {
        /// <summary>
        /// Получение списка объектов
        /// </summary>
        /// <returns>Список всех сущностей указанных в T</returns>
        List<UserViewModel> GetList();

        /// <summary>
        /// Получение объекта по Id
        /// </summary>
        /// <param name="id">Id объекта который мы хотим получить</param>
        /// <returns>Возращает объект T</returns>
        UserViewModel Get(int id);

        /// <summary>
        /// Создание объекта
        /// </summary>
        /// <param name="item">Имя объекта,который создаём</param>
        bool Create(UserViewModel item);

        /// <summary>
        /// Удаление объекта
        /// </summary>
        /// <param name="id">Id объекта,который удаляем</param>
        bool Delete(int id);

        /// <summary>
        /// Обновление объекта
        /// </summary>
        /// <param name="item">Название объекта,который обновляем</param>
        bool Update(UserViewModel item);
    }
}
