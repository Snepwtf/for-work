﻿using Registration1.BL.Interfaces;
using Registration1.BL.Interfaces.Managers;
using Registration1.BL.ViewModels;
using System.Collections.Generic;

namespace Registration1.BL.Managers
{
    public class UserManager : IUserManager
    {
        private readonly IUserRepository _userRepository;
        public UserManager(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public bool Create(UserViewModel item)
        {
            return _userRepository.Create(item);
        }

        public bool Delete(int id)
        {
            return _userRepository.Delete(id);
        }

        public UserViewModel Get(int id)
        {

            return _userRepository.Get(id);
        }

        public List<UserViewModel> GetList()
        {
            return _userRepository.GetList();
        }

        public bool Update(UserViewModel item)
        {
            return _userRepository.Update(item);
        }
    }
}
