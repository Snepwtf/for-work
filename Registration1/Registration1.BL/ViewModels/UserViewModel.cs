﻿using System;

namespace Registration1.BL.ViewModels
{
    public class UserViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string SecondName { get; set; }

        public DateTimeOffset Birthday { get; set; }

        public string Gender { get; set; }

        public int PhoneNumber { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

    }
}
