using Registration1.BL.Interfaces;
using Registration1.BL.Interfaces.Managers;
using Registration1.BL.Managers;
using Registration1.DAL.Repositories;
using System.Web.Http;
using Unity;
using Unity.WebApi;

namespace Registration1
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType<IUserManager, UserManager>();
            container.RegisterType<IUserRepository, UserRepository>();
            
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}