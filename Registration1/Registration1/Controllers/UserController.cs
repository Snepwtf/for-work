﻿using System.Web.Http;
using Registration1.BL.Interfaces.Managers;

namespace Registration1.Controllers
{
    public class UserController : ApiController
    {
        public IUserManager _userManager;

        public UserController(IUserManager userManager)
        {
            _userManager = userManager;
        }

        [HttpGet]
        public IHttpActionResult Get([FromUri] int id)
        {
            var user = _userManager.Get(id);

            if (user == null)
            {
                return BadRequest();
            }

            return Ok(user);
        }
    }
}