﻿using Registration1.DAL;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web.Http;

namespace Registration1.Controllers
{
    //[Authorize]
    public class ValuesController : ApiController
    {
        RegistrationDbContext db = new RegistrationDbContext();


        public IEnumerable<UserEntity> GetUser()
        {
            return db.Users;
        }


        public UserEntity Get(int id)
        {
            UserEntity user = db.Users.Find(id);
            return user;
        }

        [HttpPost]
        public void CreateUser([FromBody] UserEntity user)
        {
            db.Users.Add(user);
            db.SaveChanges();
        }

        [HttpPut]
        public void EditUser(int id, [FromBody]UserEntity user)
        {
            if (id == user.Id)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void DeleteUser(int id)
        {
            UserEntity user = db.Users.Find(id);
            if (user != null)
            {
                db.Users.Remove(user);
                db.SaveChanges();
            }
        }


    }
}

